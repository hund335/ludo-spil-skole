﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave.Ludo
{
    class Team
    {

        //private static String[] Teams = new String[0];
       // private static ArrayList Teams = new ArrayList();
        private static List<String> Teams = new List <String>();

        public String getTeam(int team)
        {
            return Teams[team].ToString();
        }

        public int teamSize()
        {
           
            return Teams.Count;
        }

        public bool teamisVaild(String team)
        {
            if (team.Equals("Red", StringComparison.InvariantCultureIgnoreCase) || team.Equals("Blue", StringComparison.InvariantCultureIgnoreCase) || team.Equals("Green", StringComparison.InvariantCultureIgnoreCase) || team.Equals("Yellow", StringComparison.InvariantCultureIgnoreCase))
            {
                if (!Teams.Contains(team, StringComparer.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        public void addPlayer(String team, int id)
        {
            if(teamisVaild(team) == true)
            {
                Teams.Add(team);
                
            }
        }

    }
}

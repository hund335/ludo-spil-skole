﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave.Ludo
{
    
    class Field
    {
        //private static Types get;
        private static List<Fields> pos = new List<Fields>();

        public enum Types
        {
            Home,
            Field,
            Finish,
        }
        //52 

        public struct Fields
        { 
          public int id { get; private set; }
          public String type { get; private set; }

            public Fields(int i, String s)
            {
               id = i;
               type = s;
            }

        }

        //red = 0;
        //green = 13
        //yellow = 26
        //blue = 39

        public void SetupPlayboard()
        {
   
            //Normale

            for (int i = 0; i < 52; i++)
            {
                pos.Add(new Fields(i, "Normal"));

            }
            //Globuser
            pos[0] = new Fields(0, "Globus");
            pos[8] = new Fields(8, "Globus");
            pos[13] = new Fields(13, "Globus");
            pos[21] = new Fields(21, "Globus");
            pos[26] = new Fields(26, "Globus");
            pos[34] = new Fields(34, "Globus");
            pos[39] = new Fields(39, "Globus");
            pos[47] = new Fields(47, "Globus");

            //Stjerner
            pos[5] = new Fields(5, "Stjerne");
            pos[11] = new Fields(11, "Stjerne");
            pos[18] = new Fields(18, "Stjerne");
            pos[24] = new Fields(24, "Stjerne");
            pos[31] = new Fields(31, "Stjerne");
            pos[37] = new Fields(37, "Stjerne");
            pos[44] = new Fields(44, "Stjerne");
            pos[50] = new Fields(50, "Stjerne");

            Console.WriteLine("System > Spil boardet er blevet generated med " + pos.Count + " fields");
        }

        public void homeBoard(Team t) {


        }


        public String getFieldType(int id) {

            return pos[id].type;
        }
      


    }
}
